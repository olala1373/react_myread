import React from 'react'
import { Route } from 'react-router-dom'
 import * as BooksAPI from './BooksAPI'
import './App.css'
import BookShelf from './BookShelf'
import SearchBook from './SearchBook'

class BooksApp extends React.Component {
    constructor(props){
        super(props)
        console.log('here  ')
    }
  state = {
    /**
     * TODO: Instead of using this state variable to keep track of which page
     * we're on, use the URL in the browser's address bar. This will ensure that
     * users can use the browser's back and forward buttons to navigate between
     * pages, as well as provide a good URL they can bookmark and share.
     */
    books : []
  }

  componentDidMount(){

    BooksAPI.getAll().then((books)=>{
      this.setState({books});
    })


  }

  changingShelf = (book , value, isNew) => {
      BooksAPI.update(book , value).then(() => {
          let Books = this.state.books;
          if(isNew)
              Books = Books.concat([{...book, shelf: value}])
          else
            Books =  Books.map(prevBook => prevBook.id === book.id ? {...book, shelf: value} : {...prevBook});
          this.setState({
              books : Books
          })
      })
  }

  render() {
    return (
      <div className="app">
        <Route exact path='/'  render = {()=> (
           <BookShelf
               onChangeShelf={(book, shelf) => this.changingShelf(book, shelf, false)}
               books={this.state.books}
           />
        )}/>

        <Route path='/search' render={({history})=>(
            <SearchBook
                onChangeShelf={(book, shelf) => this.changingShelf(book, shelf, true)}
                onRefresh = {() => {
                    history.push('/');

                }}
            />
        )} />

      </div>
    )
  }
}

export default BooksApp
