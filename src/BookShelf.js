import React from 'react'
import { Link } from 'react-router-dom'

const CURRENTLYREADING  = "currentlyReading";
const WANTTOREAD = "wantToRead";
const READ = "read";

class BookShelf extends React.Component {

    render(){
        const { books , onChangeShelf } = this.props;
        return(
            <div className="list-books">
                <div className="list-books-title">
                    <h1>MyReads</h1>
                </div>
                <div className="list-books-content">
                    <div>
                        <div className="bookshelf">
                            <h2 className="bookshelf-title">Currently Reading</h2>
                            <div className="bookshelf-books">
                                <ol className="books-grid">
                                    {books.filter((book)=>book.shelf === CURRENTLYREADING).map((book)=>(
                                        <li key={book.id}>
                                            <div className="book">
                                                <div className="book-top">

                                                    <div className="book-cover" style={{ width: 128, height: 193,backgroundImage: `url(${book.imageLinks.smallThumbnail})`,backgroundSize:'cover'}}></div>
                                                    <div className="book-shelf-changer">
                                                        <select onChange={(event)=>{onChangeShelf(book , event.target.value)
                                                        }}>
                                                            <option value="none" disabled>Move to...</option>
                                                            <option value="currentlyReading" selected="selected">Currently Reading</option>
                                                            <option value="wantToRead">Want to Read</option>
                                                            <option value="read">Read</option>
                                                            <option value="none">None</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="book-title">{book.title}</div>
                                                <div className="book-authors">{book.authors[0]}</div>
                                            </div>
                                        </li>
                                    ))}

                                </ol>
                            </div>
                        </div>
                        <div className="bookshelf">
                            <h2 className="bookshelf-title">Want to Read</h2>
                            <div className="bookshelf-books">
                                <ol className="books-grid">
                                    {books.filter((book)=>book.shelf === WANTTOREAD).map((book)=>(
                                        <li key={book.id}>
                                            <div className="book">
                                                <div className="book-top">

                                                    <div className="book-cover" style={{ width: 128, height: 193,backgroundImage: `url(${book.imageLinks.smallThumbnail})`,backgroundSize:'cover'}}></div>
                                                    <div className="book-shelf-changer">
                                                        <select onChange={(event)=>{onChangeShelf(book , event.target.value)

                                                        }}>
                                                            <option value="none" disabled>Move to...</option>
                                                            <option value="currentlyReading">Currently Reading</option>
                                                            <option value="wantToRead" selected="selected">Want to Read</option>
                                                            <option value="read">Read</option>
                                                            <option value="none">None</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="book-title">{book.title}</div>
                                                <div className="book-authors">{book.authors[0]}</div>
                                            </div>
                                        </li>
                                    ))}

                                </ol>
                            </div>
                        </div>
                        <div className="bookshelf">
                            <h2 className="bookshelf-title">Read</h2>
                            <div className="bookshelf-books">
                                <ol className="books-grid">
                                    {books.filter((book)=>book.shelf === READ).map((book)=>(
                                        <li key={book.id}>
                                            <div className="book">
                                                <div className="book-top">

                                                    <div className="book-cover" style={{ width: 128, height: 193,backgroundImage: `url(${book.imageLinks.smallThumbnail})`,backgroundSize:'cover'}}></div>
                                                    <div className="book-shelf-changer">
                                                        <select onChange={(event)=>{onChangeShelf(book , event.target.value)

                                                        }}>
                                                            <option value="none" disabled>Move to...</option>
                                                            <option value="currentlyReading">Currently Reading</option>
                                                            <option value="wantToRead">Want to Read</option>
                                                            <option value="read" selected> Read</option>
                                                            <option value="none">None</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="book-title">{book.title}</div>
                                                <div className="book-authors">{book.authors[0]}</div>
                                            </div>
                                        </li>
                                    ))}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="open-search">
                    <Link to="/search">
                        Open Search
                    </Link>
                </div>
            </div>//main section
        )
    }
}


export default BookShelf
/**
 * Created by mrb on 2/26/18.
 */
