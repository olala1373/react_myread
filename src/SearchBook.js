/**
 * Created by mrb on 2/26/18.
 */
import  React  from 'react'
import { Link } from 'react-router-dom'
import * as BooksAPI from './BooksAPI'
import {DebounceInput} from 'react-debounce-input';
import { withRouter } from 'react-router-dom'


let books = [];
class SearchBook extends React.Component {


    state = {
        query : ''
    }

    updateQuery  = (query) => {
        if(query === ''){
         return
        }



        BooksAPI.search(query).then((book)=>{
            books = Object.values(book);
            this.setState({query : query.trim()});
        });




    }

    render(){
        //const {query} = this.state;
        const {onChangeShelf , onRefresh} = this.props;
        return(
            <div className="search-books">
                <div className="search-books-bar">
                    <Link className="close-search" to='/'>Close</Link>
                    <div className="search-books-input-wrapper">
                        <DebounceInput
                            minLength={3}
                            debounceTimeout={400}
                            placeholder="search your desire book"
                            onChange={event => this.updateQuery(event.target.value)} />
                    </div>
                </div>
                <div className="search-books-results">
                    <ol className="books-grid">
                        {
                            books.map((book)=>(
                                <li key={book.id}>
                                    <div className="book">
                                        <div className="book-top">
                                            <div className="book-cover" style={{ width: 128, height: 193,backgroundImage: `url(${book.imageLinks.smallThumbnail})`,backgroundSize:'cover'}}></div>
                                            <div className="book-shelf-changer">
                                                <select onChange={(event) => {
                                                    onChangeShelf(book, event.target.value)
                                                    onRefresh()
                                                }}>
                                                    <option value="none" disabled>Move to...</option>
                                                    <option value="currentlyReading">Currently Reading</option>
                                                    <option value="wantToRead">Want to Read</option>
                                                    <option value="read">Read</option>
                                                    <option value="none">None</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div className="book-title">{book.title}</div>
                                        <div className="book-authors">{book.authors>0 ? book.authors[0] : 'nothing' }</div>
                                    </div>
                                </li>

                                ))}

                    </ol>
                </div>
            </div>
        )

    }
}

export default SearchBook